Started off with a basic gradient design and SVG bouncy icon and then implemented user accounts and got a bit carried away.

So in this sample application you can register, login, add a category and add a product to that category. A few possibilities of where this could go but I probably won't do anything with it.
