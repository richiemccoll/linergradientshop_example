Products = new Meteor.Collection('products');
Categories = new Meteor.Collection('categories');

Router.configure({
    layoutTemplate: 'main',
    loadingTemplate: 'loading'
});
Router.route('/', {
  name: 'home',  
  template: 'home',
  waitOn: function(){
        return Meteor.subscribe('categories');
    }
});
Router.route('/register');
Router.route('/login');
Router.route('/category/:_id', {
  name: 'categoryPage',  
  template: 'categoryPage',
      data: function(){       
          var currentCategory = this.params._id;
          var currentUser = Meteor.userId();
          return Categories.findOne({ _id: currentCategory, createdBy: currentUser });
      },
      onRun: function(){
          console.log("You triggered 'onRun' for 'categoryPage' route.");
          this.next();
      },
      onBeforeAction: function(){
            var currentUser = Meteor.userId();
            if(currentUser){
                this.next();
            } else {
                this.render("login");
            }
        },
      waitOn: function(){
          var currentCategory = this.params._id;
          return [ Meteor.subscribe('categories'), Meteor.subscribe('products', currentCategory) ]
      }
});

if(Meteor.isClient){
    Meteor.subscribe('categories');
  
    $.validator.setDefaults({
    rules: {
        email: {
            required: true,
            email: true
        },
        password: {
            required: true,
            minlength: 6
        }
    },
    messages: {
        email: {
            required: "You must enter an email address.",
            email: "You've entered an invalid email address."
        },
        password: {
            required: "You must enter a password.",
            minlength: "Your password must be at least {0} characters."
        }
    }
});  
  
    Template.register.onRendered(function(){
   var validator = $('.register').validate({
       submitHandler: function(event){
            var email = $('[name=email]').val();
            var password = $('[name=password]').val();
            Accounts.createUser({
            email: email,
            password: password
          }, function(error){
              if(error){
                  if(error.reason == "Email already exists."){
                  validator.showErrors({
                  email: "That email already belongs to a registered user."   
                  });
    }
              } else {
                  Router.go("home");
              }
          });
        }    
   });
});  
  
    Template.login.onRendered(function(){
    var validator = $('.login').validate({
        submitHandler: function(event){
        var email = $('[name=email]').val();
        var password = $('[name=password]').val();
        Meteor.loginWithPassword(email, password, function(error){
        if(error){
    if(error.reason == "User not found"){
        validator.showErrors({
            email: error.reason    
        });
    }
    if(error.reason == "Incorrect password"){
        validator.showErrors({
            password: error.reason    
        });
    }
}
          else {
           var currentRoute = Router.current().route.getName();
           if(currentRoute == "login"){
            Router.go("home");
        }
    }
        });
        }
    });
});

    Template.login.onDestroyed(function(){
    console.log("The 'login' template was just destroyed.");
});
  
    Template.categories.helpers({
    'category': function(){
        var currentUser = Meteor.userId();
        return Categories.find({ createdBy: currentUser }, {sort: {name: 1}});
    }
});

    Template.addCategory.events({
    'submit form': function(event){
      event.preventDefault();
      var categoryName = $('[name=categoryName]').val();
      Meteor.call('createNewCategory', categoryName, function(error, results){
        if(error){
            console.log(error.reason);
        } else {
            Router.go('categoryPage', { _id: results });
            $('[name=categoryName]').val('');
        }
      });
    }
});
  
    Template.login.events({
    'submit form': function(event){
        event.preventDefault();        
    }
});
  
    Template.navigation.events({
     'click .logout': function(event){
      event.preventDefault();
      Meteor.logout();
      Router.go('login');
    }
});
  
    Template.register.events({
    'submit form': function(e, template){
      event.preventDefault();    
    }    
});
  
    Template.productItem.events({
    'click .delete-todo': function(event){
    event.preventDefault();
    var documentId = this._id;
    var confirm = window.confirm("Delete this Product?");
    if(confirm){
        Meteor.call('removeCategoryItem', documentId);
    }
  },
      'keyup [name=productItemName]': function(event){
        if(event.which == 13 || event.which == 27){
        $(event.target).blur();
    }   else {
        var productNameId = this._id;
        var productItemName = $(event.target).val();
        Meteor.call('updateCategoryItemName', productNameId, productItemName);
    }},
      'keyup [name=productItemPrice]': function(event){
        if(event.which == 13 || event.which == 27){
        $(event.target).blur();
        } else {
        var productPriceId = this._id;
        var productItemPrice = $(event.target).val();
        Meteor.call('updateCategoryItemPrice', productPriceId, productItemPrice);
      }}
    });

    Template.addProduct.events({
    'submit form': function(event){
      event.preventDefault();
      var productName = $('[name="productName"]').val();
      var productPrice = $('[name="productPrice"]').val();
      var currentCategory = this._id;
      Meteor.call('createCategoryItem', productName, productPrice, currentCategory, function(error){
        if(error){
            console.log(error.reason);
        } else {
          $('[name="productName"]').val('');
          $('[name="productPrice"]').val('');
        }
      });
    }    
});
  
    Template.products.helpers({
    'product': function(){
        var currentCategory = this._id;
        var currentUser = Meteor.userId();
        return Products.find({categoryId: currentCategory, createdBy: currentUser}, {sort: {createdAt: -1}});
    }
}); 
}

if(Meteor.isServer){
  
    Meteor.methods({
    'createNewCategory': function(categoryName){
        var currentUser = Meteor.userId();
        check(categoryName, String);
        if(categoryName == ""){
            categoryName = defaultName(currentUser);
         }
        var data = {
                    name: categoryName,
                    createdBy: currentUser
                   }
        if(!currentUser){
          throw new Meteor.Error("not-logged-in", "You're not logged-in.");
    }
        return Categories.insert(data);
    },
    'createCategoryItem': function(productName, productPrice, currentCategory){
      check(productName, String);
      check (productPrice, String);
      check(currentCategory, String);
      var currentUser = Meteor.userId();
      var data = {
      name: productName,
      price: productPrice,
      createdAt: new Date(),
      createdBy: currentUser,
      categoryId: currentCategory
    };
      if(!currentUser){
        throw new Meteor.Error("not-logged-in", "You're not logged-in.");
    }
      var currentCategory = Categories.findOne(currentCategory);
      if(currentCategory.createdBy != currentUser){
          throw new Meteor.Error("invalid-user", "You don't own that list.");
      }
      return Products.insert(data);
    },
    'updateCategoryItemName': function(documentId, productItemName){
        check(productItemName, String);
        var currentUser = Meteor.userId();
        var data = { _id: documentId, createdBy: currentUser }
        if(!currentUser){
        throw new Meteor.Error("not-logged-in", "You're not logged-in.");
    }
        Products.update(data, {$set: { name: productItemName }});
    },
    'updateCategoryItemPrice': function(documentId, productItemPrice){
      check(productItemPrice, String);
      var currentUser = Meteor.userId();
      var data = { _id: documentId, createdBy: currentUser }
      if(!currentUser){
        throw new Meteor.Error("not-logged-in", "You're not logged-in.");
    }
      Products.update(data, {$set: { price: productItemPrice }});
    },
    'removeCategoryItem' : function(documentId){
        var currentUser = Meteor.userId();
        var data = { _id: documentId, createdBy: currentUser }
        if(!currentUser){
        throw new Meteor.Error("not-logged-in", "You're not logged-in.");
    }
        Products.remove(data);
    }
    });
  
    Meteor.publish('categories', function(){
      var currentUser = this.userId;
      return Categories.find({ createdBy: currentUser });
    });
  
    Meteor.publish('products', function(currentCategory){
      var currentUser = this.userId;
      return Products.find({ createdBy: currentUser, categoryId: currentCategory })
    });
  
    function defaultName(currentUser) {
      var nextLetter = 'A'
      var nextName = 'Category ' + nextLetter;
      while (Categories.findOne({ name: nextName, createdBy: currentUser })) {
        nextLetter = String.fromCharCode(nextLetter.charCodeAt(0) + 1);
        nextName = 'Category ' + nextLetter;
      }
      return nextName;
    }
}
